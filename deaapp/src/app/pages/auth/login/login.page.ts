import { Component, OnInit } from '@angular/core';
import { GrupoService } from 'src/app/services/grupo.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    signupView: boolean = false

    groups:any = [];
    register:any = {};

    constructor(private grupoS:GrupoService) { }

    ngOnInit() {
        
    }

    private getGroups(){
        this.grupoS.getCollectionGroups().subscribe((res:any)=>{
            this.groups = res.data;
        },(error)=>{
            alert(error.message);
        });
    }

    toggleSignUpView() {
        this.getGroups();
        this.signupView = !this.signupView
    }

    onSingUp(){
        this.grupoS.add(this.register).subscribe((res:any)=>{
            console.log(res);
        },(error:any)=>{
            console.log(error);
        });
    }

}
