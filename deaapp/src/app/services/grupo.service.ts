import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class GrupoService {

    constructor(private http: HttpClient) { }

    public getCollectionGroups() {
        return this.http.get('http://192.168.0.11:8000/api/grupos');
    }
    public add(data) {
        let values = {
            email: data.email,
            password: data.password,
            nombre: data.nombre,
            apellido: data.apellido,
            telefono: data.telefono,
            grupo_id: data.grupo_id
        }
        return this.http.post('http://192.168.0.11:8000/api/users',{
            values
        });
    }
}
